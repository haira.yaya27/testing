package com.haira_10191033.dashboard;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.Set;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;

    ArrayList<SetterGetter> datamenu;
    GridLayoutManager   gridLayoutManager;
    DashboardAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView            =   findViewById(R.id.rv_menu);

        addData();
        gridLayoutManager       =   new GridLayoutManager(this, 3);
        recyclerView.setLayoutManager(gridLayoutManager);
        adapter     =   new DashboardAdapter(datamenu);
        recyclerView.setAdapter(adapter);
    }


    public void addData(){

        datamenu            =   new ArrayList<>();
        datamenu.add(new SetterGetter("Pulsa", "logomenu1"));
        datamenu.add(new SetterGetter("Transfer via Bank", "logomenu2"));
        datamenu.add(new SetterGetter("My Bills", "logomenu3"));
        datamenu.add(new SetterGetter("Rewards", "logomenu4"));
        datamenu.add(new SetterGetter("Credit Card", "logomenu5"));
        datamenu.add(new SetterGetter("Games", "logomenu6"));
        datamenu.add(new SetterGetter("Water", "logomenu7"));
        datamenu.add(new SetterGetter("Electricity", "logomenu8"));
        datamenu.add(new SetterGetter("Nearby", "logomenu9"));


    }
}